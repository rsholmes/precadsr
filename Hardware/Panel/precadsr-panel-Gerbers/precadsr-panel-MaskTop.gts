G04 #@! TF.GenerationSoftware,KiCad,Pcbnew,7.0.11-7.0.11~ubuntu22.04.1*
G04 #@! TF.CreationDate,2024-03-05T20:19:51-05:00*
G04 #@! TF.ProjectId,precadsr-panel,70726563-6164-4737-922d-70616e656c2e,rev?*
G04 #@! TF.SameCoordinates,Original*
G04 #@! TF.FileFunction,Soldermask,Top*
G04 #@! TF.FilePolarity,Negative*
%FSLAX46Y46*%
G04 Gerber Fmt 4.6, Leading zero omitted, Abs format (unit mm)*
G04 Created by KiCad (PCBNEW 7.0.11-7.0.11~ubuntu22.04.1) date 2024-03-05 20:19:51*
%MOMM*%
%LPD*%
G01*
G04 APERTURE LIST*
%ADD10O,6.800000X4.000000*%
%ADD11C,14.000000*%
%ADD12C,9.000000*%
%ADD13C,17.600000*%
%ADD14C,5.500000*%
G04 APERTURE END LIST*
D10*
G04 #@! TO.C,H1*
X4400000Y197000000D03*
X4400000Y3000000D03*
G04 #@! TD*
G04 #@! TO.C,H2*
X45600000Y197000000D03*
X45600000Y3000000D03*
G04 #@! TD*
D11*
G04 #@! TO.C,H3*
X13000000Y75000000D03*
G04 #@! TD*
D12*
G04 #@! TO.C,H4*
X37000000Y170000000D03*
G04 #@! TD*
G04 #@! TO.C,H5*
X13000000Y170000000D03*
G04 #@! TD*
D11*
G04 #@! TO.C,H6*
X13000000Y50000000D03*
G04 #@! TD*
D12*
G04 #@! TO.C,H7*
X37000000Y135000000D03*
G04 #@! TD*
G04 #@! TO.C,H8*
X13000000Y135000000D03*
G04 #@! TD*
D11*
G04 #@! TO.C,H9*
X13000000Y25000000D03*
G04 #@! TD*
D12*
G04 #@! TO.C,H10*
X37000000Y100000000D03*
G04 #@! TD*
D13*
G04 #@! TO.C,H11*
X13000000Y100000000D03*
G04 #@! TD*
D11*
G04 #@! TO.C,H12*
X37000000Y25000000D03*
G04 #@! TD*
D12*
G04 #@! TO.C,H13*
X37000000Y65000000D03*
G04 #@! TD*
D14*
G04 #@! TO.C,H14*
X25000000Y25000000D03*
G04 #@! TD*
M02*
